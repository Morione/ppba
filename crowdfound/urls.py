from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns


from crowdfound import settings

urlpatterns = [
    url(r'^', include('gui.urls')),
    url(r'^core/', include('core.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]
urlpatterns += staticfiles_urlpatterns()\
               + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
