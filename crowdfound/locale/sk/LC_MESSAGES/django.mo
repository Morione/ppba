��    *      l  ;   �      �     �     �  
   �     �  
   �     �                    /     C     T     f     z     �     �     �  
   �     �     �     �  
   �               *     E     R  
   ^     i     ~     �     �     �     �     �     �     �     �               "  �  1     �     �     �     �       D         ]     ~     �     �  	  �     �	     �	  (   �	  d   �	  8   V
     �
  u   �
  @        S     _     k     ~     �     �     �     �     �  �   �     a     o     �     �      �     �      �     	          >     M     c            $         %                 
                        	   #            *   "   )   &         !                                                        (         '                                      home.actual.token home.all.token home.hello home.invest.token home.login home.login.title home.thankyou home.tokens intro.button.next intro.h1.investment intro.title.text logged-out.logout login.button.choose login.button.choose.title login.description login.error.access login.h1.title login.info login.intro login.invest login.logout login.name login.name.placeholder login.password login.password.placeholder login.submit login.title qr.alt.img qr.check.description qr.check.link qr.check.no qr.check.ok qr.label.btc qr.label.username qr.password.username qr.password2.username qr.show.account qr.text.later qr.title qr.title.btc qr.title.check Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-12-18 09:29+0100
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Aktuálna hodnota [€] Vysledná hodnota [€] Ahoj Investované prostriedky [€] Prihlásiť Ahoj, pre prezeranie profilu je potrebné sa prihlásiť do systému Ďakujeme za prejavenú podporu. Zakúpené tokeny Pokračovať Investovanie Zakúpením tokenu podporíte projekt Paralelní Polis Bratislava a zároveň zhodnotíte výšku Vašej investície. Výška investovaných prostriedkov je len na Vás. V prvom kroku je potrebné zadať bitcoin adresu na ktorú sa do budúcna vyplatí zhodnotenie. Odhlásenie Vybrať Investíciu nie je ešte možné vybrať Stránka, na ktorej sa práve nachádzate slúži pre potreby investovania do projektu PP Bratislava Nepodarilo sa prihlásiť. Zle zadané meno alebo heslo! Dobrý deň! Prostredníctvom zakúpených tokenov je možné podporiť a zároveň investovať do myšlienky Paralelního Polisu. Myšlienku som už podporil/a a chcem sa prihlásiť do systému Investovať Odhlásenie Prihlasovacie meno hash Prihlasovacie heslo heslo Prihlásiť Prihlásenie QR kód BTC adresy na príjem Po zaplatení čiastky, skontrolujte potvrdenie tranzakcie stlačením tlačidla 'Skontrolovať', alebo na aktuálne načítanej URL adrese. Skontrolovať Platba zatiaľ nepotvrdená Platba potvrdená BTC adresa na príjem Vygenerované prihlasovacie meno Prihlasovacie heslo Potvrdenie prihlasovacieho hesla Pozrieť svoj účet Skontrolujte platbu neskoršie. Príjem vkladu BTC adresa na príjem Overenie platby 