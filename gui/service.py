import os

import requests
import logging

from core.models import Token
from crowdfound import settings
from gui.addrgen import derive_address
from gui.exceptions import QrCodeException, BtcAddressException

log = logging.getLogger(__name__)


def create_btc_qr(btc, user):
	assert btc, 'missing btc address for receive payment'
	assert user, 'missing user name and password'
	idx = __get_idx_address()
	new_btc_address = __generate_request_btc_adress(idx)
	log.info("create btc address %s [%d]", new_btc_address, idx)
	__generate_qrcode(new_btc_address)
	return Token.objects.create(btc_account=btc,
								btc_payment_account=new_btc_address,
								user=user)


def __get_idx_address():
	return Token.objects.count() + 1


def __generate_qrcode(adress):
	""" from btc adress generate png image. Filename string will be return"""

	api_url = "https://chart.googleapis.com/chart?"
	size = "250x250"
	generate_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + settings.MEDIA_URL
	file_name = adress + ".png"
	url = api_url + 'chs=' + size + '&cht=qr' + '&chl=' + adress
	try:
		r = requests.get(url)
		# create image from api response
		image_file = open(generate_path + file_name, "wb")
		image_file.write(r.content)
		image_file.close()
		return file_name
	except Exception:
		raise QrCodeException('problem generated qr code from url %s', url)


def __generate_request_btc_adress(idx):
	try:
		address = derive_address(settings.MAIN_BTC_WALLET, [0, idx])
		return address
	except Exception:
		raise BtcAddressException('problem generated address')


def create_balance_update_notification_request(
		token,
		api_key=settings.BLOCKCHAIN_API_KEY
):
	callback_url = settings.CALLBACK_URL
	secret = settings.TOKEN_SECRET_KEY
	callback = callback_url + "/?id=" + str(token.id)
	url = "https://api.blockchain.info/v2/receive/balance_update"
	req = requests.post(url,
						headers={"Content-Type": "text/plain"},
						data='{\
							"key": "' + api_key + '", \
							"addr": "' + token.btc_payment_account + '",\
							"callback": "' + callback + '",\
							"onNotification":"KEEP",\
							"op":"RECEIVE",\
							"confs": "3" \
						}'
						)
	log.debug(req.content)
	return True if req.status_code == 200 else False
