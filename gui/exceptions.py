

class QrCodeException(Exception):
	"""Raise if is problem in generated QR code """
	pass


class BtcAddressException(Exception):
	"""Raise if is problem in generated BTC address """
	pass
