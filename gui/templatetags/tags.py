from decimal import Decimal

from django import template

register = template.Library()


@register.filter(name='actual_balance')
def actual_balance(token):
	rate = Decimal(token.invest_amount) * Decimal(0.01) * Decimal(token.interest_rate)
	result = round(token.invest_amount + rate - token.payout, 2)
	return str(result)
