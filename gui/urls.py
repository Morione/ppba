from django.conf.urls import url
from django.contrib.auth import views as auth_views
from gui.views import TokenView, TransactionView, NotificationView, TokenCreateView, IntroView, \
	TokenCheckView, HomeView, LanguageView

urlpatterns = [

	url(r'^$', auth_views.LoginView.as_view(), name='login'),
	url(r'^buy/$', IntroView.as_view(), name='intro'),
	url(r'^accounts/profile/$', HomeView.as_view(), name='home'),
	url(r'^accounts/logout/$', auth_views.LogoutView.as_view(), name='logout'),
	url(r'^token/$', TokenView.as_view(), name='gui-token-list'),
	url(r'^new/$', TokenCreateView.as_view(), name='token-new'),
	url(r'^check/(?P<address>[^/]+)/$', TokenCheckView.as_view(), name='token-check'),
	url(r'^transaction/$', TransactionView.as_view(), name='gui-transaction-list'),
	url(r'^notification/$', NotificationView.as_view(), name='notification'),
	url(r'^lang/(?P<lang>[^/]+)/$', LanguageView.as_view(), name='lang'),
]