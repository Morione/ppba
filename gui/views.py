import json
import logging
import uuid
from decimal import Decimal

from django.contrib.auth import authenticate
from django.contrib.auth.views import login
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from django.http import HttpResponse
from django.http import HttpResponseForbidden
from django.shortcuts import render, redirect
from django.views.generic import CreateView
from django.views.generic import ListView, View
from django.views.generic import TemplateView
from django.utils import translation

from core.models import Transaction, Token
from gui.forms import RegistrationForm
from gui.service import create_btc_qr, create_balance_update_notification_request

log = logging.getLogger(__name__)


class LanguageView(View):

	def get(self, request,  *args, **kwargs):
		if kwargs['lang']:
			translation.activate(kwargs['lang'])
			request.LANGUAGE_CODE = translation.get_language()
			request.session[translation.LANGUAGE_SESSION_KEY] = request.LANGUAGE_CODE
		return redirect('login')


class IntroView(TemplateView):
	""" View for btc investment """

	def get(self, request, *args, **kwargs):
		log.debug('IntroView loaded')
		return render(request, 'core/intro.html', {'form': self.__init_form()})

	@transaction.atomic
	def post(self, request):
		log.debug('IntroView received request')
		form = RegistrationForm(request.POST)

		if form.is_valid():
			form.save()
			username = form.cleaned_data.get('username')
			raw_password = form.cleaned_data.get('password1')
			user = authenticate(username=username, password=raw_password)
			user.backend = 'django.contrib.auth.backends.ModelBackend'
			login(request, user)
			log.info('authenticate successfull for ', user)

			token = create_btc_qr(request.POST['btc'], user)
			if create_balance_update_notification_request(token):
				log.info('create notification for payment %s', token.btc_payment_account)
				return render(request, 'core/qr.html', {'token': token})

		return render(request, 'core/intro.html', {'form': self.__init_form(form)})
			# def get(self, request, *args, **kwargs):
			# 	token = Token(btc_account="xxx",btc_payment_account="Xxx",valid=1)
			# 	return render(request, 'core/qr.html', {'token': token})

	def __init_form(self, form=None):
		if not form:
			form = RegistrationForm()
		form.fields["username"].initial = uuid.uuid4()
		form.fields["username"].widget.attrs['class'] = 'form-control'
		form.fields["password1"].widget.attrs['class'] = 'form-control'
		form.fields["password2"].widget.attrs['class'] = 'form-control'
		return form


class HomeView(TemplateView):
	""" View of account profile management"""

	template_name = 'core/home.html'

	def get(self, request, *args, **kwargs):

		log.debug('HomeView loaded')
		if request.user.is_authenticated():

			log.info('Authorized user %s', request.user)
			if request.user.is_superuser:
				log.debug('User is superuser')
				return redirect('gui-token-list')
			tokens = Token.objects.filter(user=request.user)

		return render(request, self.template_name, {'tokens': tokens})


class TokenCheckView(TemplateView):
	""" Token check view """

	template_name = 'core/qr.html'

	def get(self, request, *args, **kwargs):
		log.debug('TokenCheckView loaded')
		try:
			token = Token.objects.get(btc_payment_account=kwargs['address'])
		except ObjectDoesNotExist:
			log.error('BTC address [%s] is not valid', kwargs['address'])
			#todo: sprava o tom, ze url je neplatna
			return redirect('login')

		return render(request, self.template_name, {'token': token})

	# def post(self, request, *args, **kwargs):
	#
	# 	log.debug('TokenCheckView received request')
	# 	form = UserCreationForm(request.POST)
	# 	if form.is_valid():
	# 		form.save()
	# 		username = form.cleaned_data.get('username')
	# 		raw_password = form.cleaned_data.get('password1')
	# 		user = authenticate(username=username, password=raw_password)
	# 		login(request, user)
	# 		log.info('authenticate successfull for ', user)
	# 		return redirect('home')
	# 	else:
	# 		form.fields["username"].initial = kwargs['address']
	# 		form.fields["username"].widget.attrs['size'] = 40
	#
	# 	return render(request, 'core/qr.html', {
	# 		'token': Token.objects.get(btc_payment_account=kwargs['address']),
	# 		'form': form
	# 	})


class NotificationView(View):
	""" Not GUI view. View is for handling notification request after btc payment """

	def post(self, request, *args, **kwargs):

		log.debug('NotificationView loaded')
		data = json.loads(request.body)

		#val = Decimal(values['value']) / 100000000

		if 'address' in data:
			#settings.TOKEN_SECRET_KEY == values['secret']:

			token = Token.objects.get(btc_payment_account=data['address'])
			token.valid = True
			token.invest_amount = Decimal(str(data['value']))

			#todo: transaction_hash
			#if 'transaction_hash' in values:
				#token.transaction_hash = data['transaction_hash']

			token.save()
			log.info('Save token %s', token)

			return HttpResponse('ok')
		else:
			return HttpResponseForbidden()


class TransactionView(ListView):
	""" View for showing list of all transactions """

	model = Transaction
	template_name = 'core/transaction/transaction_list.html'


# ONLY FOR TESTING PURPOUSE
class TransactionCreateView(CreateView):
	""" Creating some test transaction """

	model = Transaction
	template_name = 'core/transaction/transaction_new.html'
	fields = ['amount', 'saved_rate']


class TokenView(ListView):
	""" View for showing list of all tokens """

	model = Token
	template_name = 'core/token/token_list.html'


# ONLY FOR TESTING PURPOUSE
class TokenCreateView(CreateView):
	""" Creating some test token """

	model = Token
	template_name = 'core/token/token_new.html'
	fields = [
		'invest_amount',
		'btc_account',
		'interest_rate',
	]