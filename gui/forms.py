from hashlib import sha256

from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.core.exceptions import ValidationError


def validate_btc(value):
	if not __check_btc(str(value)):
		raise ValidationError(
			'%(value)s is not correct BTC address',
			params={'value': value},
		)


class RegistrationForm(UserCreationForm):

	btc = forms.CharField(
		label="BTC payment address",
		help_text='BTC payment address',
		widget=forms.widgets.Input(
			attrs={'class': 'form-control', 'autofocus': 'autofocus'}),
		validators=[validate_btc],
	)

	def __init__(self, *args, **kwargs):
		super(RegistrationForm, self).__init__(*args, **kwargs)


def __check_btc(bc):
	try:
		bcbytes = __decode_base58(bc, 25)
		return bcbytes[-4:] == sha256(sha256(bcbytes[:-4]).digest()).digest()[:4]
	except Exception:
		return False


def __decode_base58(bc, length):
	digits58 = '123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz'
	n = 0
	for char in bc:
		n = n * 58 + digits58.index(char)
	return n.to_bytes(length, 'big')
