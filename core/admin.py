from django.contrib import admin
from .models import Transaction, Token

admin.site.register(Transaction)
admin.site.register(Token)