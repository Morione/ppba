from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt
from rest_framework.urlpatterns import format_suffix_patterns

from core import views
from .views import TokenApiView, TokenApiListView, TransactionApiView, TransactionApiListView

urlpatterns = [

	url(r'^$', csrf_exempt(TokenApiListView.as_view()), name='token-list'),

	url(r'^token/$', csrf_exempt(TokenApiListView.as_view()), name='token-list'),
	url(r'^token/(?P<pk>[^/]+)/$', csrf_exempt(TokenApiView.as_view()), name='token'),
	url(r'^transaction/$', csrf_exempt(TransactionApiListView.as_view()), name='transaction-list'),
	url(r'^transaction/(?P<pk>[^/]+)/$', csrf_exempt(TransactionApiView.as_view()), name='transactions'),

	url(r'^users/$', views.UserList.as_view()),
	url(r'^user/(?P<pk>[0-9]+)/$', views.UserDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
