from django.test import TestCase

from core.models import Token, Transaction
from .addrgen import addr_from_mpk
from crowdfound.settings import MPK


class WebSanityTests(TestCase):
	def test_index_page_status_code(self):
		response = self.client.get('/')
		self.assertEquals(response.status_code, 200)

	def test_token_page_status_code(self):
		response = self.client.get('/token/')
		self.assertEquals(response.status_code, 200)

	def test_transaction_page_status_code(self):
		response = self.client.get('/transaction/')
		self.assertEquals(response.status_code, 200)


class ModelTests(TestCase):

	def test_create_tokens(self):
		token = Token.objects.create(
			invest_amount=1000, interest_rate=5.00, btc_account="SK001100123456")
		self.assertEqual(float(token.balance), float(1050), "vypocet uroku")

		token = Token.objects.create(
			invest_amount=333, interest_rate=10.00, btc_account="SK001100123456")
		self.assertEqual(float(token.balance), float(366.3), "vypocet uroku")

	def test_create_transactions(self):
		transaction = Transaction.objects.create(amount=23.00, saved_rate=10)
		self.assertEqual(float(transaction.saved_amount), float(2.3), "vypocet odvodu")

		transaction = Transaction.objects.create(amount=123.52, saved_rate=10)
		self.assertEqual(float(transaction.saved_amount), float(12.352), "vypocet odvodu")

		transaction = Transaction.objects.create(amount=0.1, saved_rate=5)
		self.assertEqual(float(transaction.saved_amount), float(0.005), "vypocet odvodu")


class BtcGenerator(TestCase):

	def test_generate_btc_address(self):
		addr = addr_from_mpk(b"02244cf6ac4da4e85c2188d6d2870230156a5d95d00df5692ae9283eae44a620fc".decode("utf8"), 0)
		print(addr)