import urllib
import uuid

from decimal import Decimal

import datetime
from django.contrib.auth.models import User
from django.db import models
from django.db.models import Sum
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.urls import reverse


class Token(models.Model):
	""" token of investement """

	id = models.UUIDField(
		primary_key=True, default=uuid.uuid4, editable=False)

	invest_satoshi_btc = models.DecimalField(
		max_digits=12, decimal_places=2, blank=False, null=False, default=0,
		help_text="invest amount in satoshi btc")

	invest_amount = models.DecimalField(
		max_digits=7, decimal_places=2, blank=False, null=False, default=0,
		help_text="invest amount in euro")

	btc_account = models.TextField(
		blank=False, null=False, help_text="btc account for receive payment", default="")

	btc_payment_account = models.TextField(
		blank=False, null=False, help_text="btc account for payment", default="")

	interest_rate = models.DecimalField(
		max_digits=5, decimal_places=2, blank=False, null=False, default=5,
		help_text="inicial rate in percent")

	balance = models.DecimalField(
		max_digits=9, decimal_places=4, blank=False, null=False, default=0,
		help_text="balance of account in euro")

	payout = models.DecimalField(
		max_digits=9, decimal_places=4, blank=False, null=False, default=0,
		help_text="unpaid dividend")

	create_time = models.DateTimeField(
		auto_now=True, help_text="time of token creation")

	withdraw_time = models.DateTimeField(
		default=None, blank=True, null=True, help_text="time of paid dividend")

	valid = models.BooleanField(default=False, blank=False, null=False, help_text="valid token")

	user = models.ForeignKey(User, default=1, help_text="owner of token")

	def __str__(self):
		"""A string representation of the model."""
		return "{} : {}".format(self.id, self.invest_amount)

	@staticmethod
	def get_absolute_url():
		return reverse('token-list')


@receiver(pre_save, sender=Token)
def token_save_handler(sender, instance, **kwargs):
	""" handler pre vypocet poctu jednotiek pomocou uroku z investovanej sumy """
	if instance.balance == 0:

		# btc to euro
		#todo: nerobit pri kazdej tranzakcii ale drzat kurz niekde zakesovany
		invest_amount_eur = get_current_eur_by_btc(instance.invest_satoshi_btc)
		payout = Decimal(invest_amount_eur) * Decimal(instance.interest_rate) * Decimal(0.01)
		instance.balance = Decimal(invest_amount_eur) + Decimal(payout)
		instance.payout = payout
		instance.invest_amount = invest_amount_eur


def get_current_eur_by_btc(satoshi_btc, symbol='EUR'):
	""" Getting current value of btc by symbol. Latency is 15 minutes"""
	url = 'https://blockchain.info/tobtc?currency='+symbol+'&value=' + str(satoshi_btc)
	resource = urllib.request.urlopen(url)
	# fixme: ak zliha sluzba tretej strany, tak osetrit
	return Decimal(resource.read().decode(resource.headers.get_content_charset()))



class Transaction(models.Model):
	""" transaction of sale coffee and other staffs """

	id = models.UUIDField(
		primary_key=True, default=uuid.uuid4, editable=False)

	amount_satoshi_btc = models.DecimalField(
		max_digits=12, decimal_places=2, blank=False, null=False, default=0,
		help_text="amount of transaction in satoshi")

	saved_rate = models.DecimalField(
		max_digits=5, decimal_places=2, blank=False, null=False, default=10,
		help_text="rate to found in percent")

	saved_amount = models.DecimalField(
		max_digits=9, decimal_places=4, blank=False, null=False, default=0,
		help_text="amount to found in euro")

	calculation_time = models.DateTimeField(
		default=None, blank=True, null=True, help_text="calculation time")

	create_time = models.DateTimeField(
		auto_now=True, help_text="transaction time")

	def __str__(self):
		"""A string representation of the model."""
		return "{} : {} : {}".format(self.id, self.amount, self.saved_amount)

	@staticmethod
	def get_absolute_url():
		return reverse('transaction-list')


@receiver(pre_save, sender=Transaction)
def transaction_save_handler(sender, instance, **kwargs):
	""" handler for transaction """
	if instance.saved_amount == 0:

		# vypocet velkosti ciastky z predaje urcenej k splateniu pohladavok

		amount_eur = get_current_eur_by_btc(instance.amount_satoshi_btc)
		instance.saved_amount = Decimal(amount_eur) * Decimal(instance.saved_rate) * Decimal(0.01)
		instance.calculation_time = datetime.datetime.now()

		# zistit koeficient
		payout_sum = Token.objects.aggregate(Sum('payout'))['payout__sum']
		if not payout_sum:
			print('sum is none')
			return

		k = Decimal(instance.saved_amount) / Decimal(payout_sum)

		# kazdemu investori sa splaca urok z tranzakcie
		for token in Token.objects.all():
			token.payout = Decimal(token.payout) - Decimal(k * token.payout)
			print('payout: ' + str(token.payout))
			if token.payout < 0:
				token.payout = 0
				token.withdraw_time = datetime.datetime.now()
			token.save()
