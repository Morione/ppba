# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-10-16 12:05
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='token',
            name='interest_rate',
            field=models.DecimalField(decimal_places=2, default=0, help_text='pociatocny urok %', max_digits=7),
        ),
        migrations.AlterField(
            model_name='token',
            name='invest_amount',
            field=models.DecimalField(decimal_places=2, default=0, help_text='suma vkladu (istina)', max_digits=7),
        ),
        migrations.AlterField(
            model_name='token',
            name='payout',
            field=models.DecimalField(decimal_places=2, default=0, help_text='nevyplatena suma', max_digits=7),
        ),
        migrations.AlterField(
            model_name='token',
            name='withdraw_time',
            field=models.DateTimeField(blank=True, default=None, help_text='cas vyplatenia tokenu', null=True),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='amount',
            field=models.DecimalField(decimal_places=2, default=0, help_text='suma tranzakcie', max_digits=7),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='saved_amount',
            field=models.DecimalField(decimal_places=2, default=0, help_text='ciastka do fondu', max_digits=7),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='saved_rate',
            field=models.DecimalField(decimal_places=2, default=0, help_text='urok do fondu %', max_digits=7),
        ),
    ]
