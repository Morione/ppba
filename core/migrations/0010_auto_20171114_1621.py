# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-11-14 16:21
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0009_auto_20171114_1607'),
    ]

    operations = [
        migrations.AlterField(
            model_name='token',
            name='balance',
            field=models.DecimalField(decimal_places=2, default=0, help_text='pocet jednotiek', max_digits=7),
        ),
    ]
