from django.contrib.auth.models import User
from rest_framework import serializers

# Serializers define the API representation.
from .models import Token, Transaction


class UserSerializer(serializers.HyperlinkedModelSerializer):

	class Meta:
		model = User
		fields = ('url', 'username', 'email', 'is_staff')


class TokenSerializer(serializers.ModelSerializer):

	class Meta:
		model = Token
		fields = ('id', 'invest_satoshi_btc', 'btc_account', 'interest_rate')


class TransactionSerializer(serializers.ModelSerializer):

	class Meta:
		model = Transaction
		fields = ('id', 'amount_satoshi_btc', 'saved_rate')

