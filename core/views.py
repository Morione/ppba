from django.contrib.auth.models import User
from rest_framework import generics
from rest_framework import permissions
from rest_framework.authentication import SessionAuthentication, BasicAuthentication

from core.serializers import TokenSerializer, TransactionSerializer, UserSerializer
from .models import Transaction, Token


class TokenApiListView(generics.ListCreateAPIView):

	queryset = Token.objects.all()
	serializer_class = TokenSerializer


class TokenApiView(generics.RetrieveUpdateDestroyAPIView):

	queryset = Token.objects.all()
	serializer_class = TokenSerializer
	authentication_classes = (SessionAuthentication, BasicAuthentication)
	permission_classes = [permissions.IsAuthenticatedOrReadOnly]


class TransactionApiListView(generics.ListCreateAPIView):

	queryset = Transaction.objects.all()
	serializer_class = TransactionSerializer


class TransactionApiView(generics.RetrieveUpdateDestroyAPIView):

	queryset = Transaction.objects.all()
	serializer_class = TransactionSerializer
	authentication_classes = (SessionAuthentication, BasicAuthentication)
	permission_classes = [permissions.IsAuthenticatedOrReadOnly]


class UserList(generics.ListAPIView):
	queryset = User.objects.all()
	serializer_class = UserSerializer


class UserDetail(generics.RetrieveAPIView):
	queryset = User.objects.all()
	serializer_class = UserSerializer