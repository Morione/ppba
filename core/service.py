from .models import Token, Transaction


def delete_all_tokens():
	""" remove all tokens """
	Token.objects.all().delete()
	return True


def delete_all_transactions():
	""" remove all transactions """
	Transaction.objects.all().delete()
	return True
